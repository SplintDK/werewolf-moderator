import Enumerations.RoleType;

import java.util.Map;

public interface IGame {

    /**
     *
     * @param inputRoles Map of RoleType and Integers which defines how many of each role is in the game.
     * @return The Game class itself.
     */
    Game setupRoles(Map<RoleType, Integer> inputRoles);

    /**
     * Wakes up every role which has been defined in setupRoles which allows the Moderator to
     * map all characters to all roles. Also does a normal night phase.
     */
    void firstNight();


    /**
     * Wakes up roles with nightly abilities
     */
    void night();

    void playerSelection(Character character);
}
