package Enumerations;

public enum PlayerState {
    DEAD,
    ALIVE,
    LYNCH_KILL,
    BODYGUARD_PROTECTED
}
