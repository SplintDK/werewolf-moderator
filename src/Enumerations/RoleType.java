package Enumerations;

public enum RoleType {

    IDIOT ("Idiot"),
    PRIEST("Priest"),
    MARTYR("Martyr"),
    PI("P.I."),
    CULT_LEADER("Cult Leader"),
    TANNER("Tanner"),
    PACIFIST("Pacifist"),
    HOODLUM("Hoodlum"),
    SPELLCASTER("Spellcaster"),
    AURA_SEER("Aura Seer"),
    TROUBLEMAKER("Troublemaker"),
    PRINCE("Prince"),
    CUPID("Cupid"),
    OLD_MAN("Old Man"),
    DOPPEL_GANGER("Doppelganger"),
    CURSED("Cursed"),
    GHOST("Ghost"),
    DRUNK("Drunk"),
    SORCERER("Sorcerer"),
    LYCAN("Lycan"),
    MINION("Minion"),
    BODYGUARD("Bodyguard"),
    OLD_HAG("Old Hag"),
    MAYOR("Mayor"),
    HUNTER("Hunter"),
    DISEASED("Diseased"),
    TOUGH_GUY("Tough Guy"),
    LONE_WOLF("Lone Wolf"),
    WOLF_CUB("Wolf Cub"),
    WITCH("Witch"),
    APPRENTICE_SEER("Apprentice Seer"),
    MAGICIAN("Magician");

    private String roleText;

    RoleType(String role) {
        this.roleText = role;
    }

    public String getRoleText(){
        return roleText;
    }

    public static RoleType fromString(String text){
        for (RoleType roleType : RoleType.values()) {
            if (roleType.roleText.equalsIgnoreCase(text))
                return roleType;
        }
        return null;
    }
}
