package Enumerations;

public enum Affiliation {
    WEREWOLF,
    VILLAGER,
    VAMPIRE,
    CULT,
    SOULMATE
}
