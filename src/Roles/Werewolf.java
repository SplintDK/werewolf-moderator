package Roles;

import Enumerations.Affiliation;
import Enumerations.PlayerState;
import Models.Character;
import Roles.Role;

import java.util.ArrayList;
import java.util.List;

public class Werewolf extends Role {

    public Werewolf(){
        affiliationList = new ArrayList<>();
        affiliationList.add(Affiliation.WEREWOLF);
        name = "Werewolf";
        description = "Wake with the other werewolves each night and choose 1 villager to eat.";
    }

    @Override
    public void nightlyAbility(List<Character> targets) {
        for(Character target : targets){
            if(target.getPlayerState() == PlayerState.BODYGUARD_PROTECTED){
                continue;
            }else {
                target.setPlayerState(PlayerState.DEAD);
            }
        }
    }
}
