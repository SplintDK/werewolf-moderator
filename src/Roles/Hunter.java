package Roles;

import Enumerations.Affiliation;
import Enumerations.PlayerState;
import Models.Character;

import java.util.ArrayList;
import java.util.List;

public class Hunter extends Role {

    public Hunter(){
        affiliationList = new ArrayList<>();
        affiliationList.add(Affiliation.VILLAGER);
        name = "Hunter";
        description = "If you die, you may immediately kill 1 another player.";
    }

    @Override
    public void onDeath(List<Character> targets) {
        if(targets.size() > 0)
            targets.get(0).setPlayerState(PlayerState.DEAD);
    }
}
