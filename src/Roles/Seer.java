package Roles;

import Enumerations.Affiliation;
import Models.Character;

import java.util.ArrayList;
import java.util.List;

public class Seer extends Role {

    public Seer(){
        affiliationList = new ArrayList<>();
        affiliationList.add(Affiliation.VILLAGER);
        name = "Seer";
        description = "Each night choose 1 player to learn if they are a villager or werewolf.";
    }

    @Override
    public void nightlyAbility(List<Character> targets) {
        for( Affiliation affiliation : targets.get(0).getRole().affiliationList){
            System.out.println(affiliation);
        }
    }
}
