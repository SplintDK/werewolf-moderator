package Roles;

import Enumerations.Affiliation;
import Enumerations.PlayerState;
import Models.Character;

import java.util.List;

public abstract class Role {
    protected List<Affiliation> affiliationList;
    protected String name;
    protected String description;

    public Role() {

    }

    public List<Affiliation> getAffiliationList() {
        return affiliationList;
    }

    public void setAffiliationList(List<Affiliation> affiliationList) {
        this.affiliationList = affiliationList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void nightlyAbility(List<Character> targets){
        //Jack shit, just returning for fun
        return;
    }

    public void firstNight(List<Character> targets){
        nightlyAbility(targets);
    }

    public void onDeath(List<Character> targets){

    }
}
