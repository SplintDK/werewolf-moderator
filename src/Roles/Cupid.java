package Roles;

import Enumerations.Affiliation;
import Models.Character;

import java.util.ArrayList;
import java.util.List;

public class Cupid extends Role{

    public Cupid(){
        affiliationList = new ArrayList<>();
        affiliationList.add(Affiliation.VILLAGER);
        name = "Cupid";
        description = "Choose 2 players to be lovers. If one of those players dies, the other dies from a broken heart.";
    }

    @Override
    public void firstNight(List<Character> targets) {
        if(targets.size() == 2){
            for(Character target : targets)
                target.getRole().affiliationList.add(Affiliation.SOULMATE);
        }
    }
}
